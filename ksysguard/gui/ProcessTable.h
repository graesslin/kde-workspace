/*
    KSysGuard, the KDE System Guard
   
    Copyright (c) 2009 - John Tapsell <tapsell@kde.org>
    
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public
    License version 2 or at your option version 3 as published by
    the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#ifndef PROCESS_TABLE_H_VARS__
#define PROCESS_TABLE_H_VARS__

class ProcessController;

/* This is the process list widget */
extern ProcessController *sLocalProcessController;

#endif
